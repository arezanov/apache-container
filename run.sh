#!/bin/bash

RESTART_DELAY=60

case $1 in
    start)
        chmod 755 /
        service rsyslog start
        service apache2 start
        while true; do
            sleep 3600
        done
        ;;
    stop)
        service rsyslog stop
		service apache2 stop
        ;;
    *)
        # точка входа в скрипт (при вызове без параметров)
        echo "`date +\"%Y-%m-%dT%H:%M:%S%:z\"` Container started"
        # перехватываем сигналы
        trap 'echo "`date +\"%Y-%m-%dT%H:%M:%S%:z\"` SIGINT received"; /run.sh stop; exit 0' INT
        trap 'echo "`date +\"%Y-%m-%dT%H:%M:%S%:z\"` SIGTERM received"; /run.sh stop; exit 0' TERM
        /run.sh start &
        wait $!
        exit $?
        ;;
esac
