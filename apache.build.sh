#!/bin/bash

set -e

echo -n "Container name: apache-"
read name 

echo -n "Mounting port: "
read  port

echo -n "Imported www directory (empty for none): "
read volume_local
if [ -n "$volume_local" ]; then
    volume_opt="--volume=${volume_local}:/var/www/html"
fi

docker build . --tag="apache-${name}"

docker run -d -p 127.0.0.1:${port}:80 \
            --name="apache-${name}" \
            --hostname="apache-${name}" \
            --restart=unless-stopped \
	    ${volume_opt} \
            apache-${name}

