FROM httpd:2.4

RUN rm -f /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y rsyslog mc net-tools dnsutils wget
RUN apt-get install -y php php-mbstring php-xml
RUN apt-get install -y libapache2-mod-php php-xml
RUN a2dismod -f autoindex

COPY ./run.sh /run.sh
RUN chmod u+x /run.sh

WORKDIR /etc/apache2

ENTRYPOINT ["/run.sh"]
